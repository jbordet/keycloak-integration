package com.jbdev.library.keycloakintegration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class KeycloakLoginResponse {

    private String access_token;
    private Long expires_in;
    private Long refresh_expires_in;
    private String refresh_token;
    private String tokenType;
    private Long notBeforePolicy;
    private String session_state;
    private String scope;

}
