package com.jbdev.library.keycloakintegration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.BodyInserters;


import java.time.Instant;
import java.util.Objects;

@Component
@Getter
@Setter
@NoArgsConstructor
public class ApplicationJwtService {

    private Jwt jwt;

    private WebClient.Builder webClientBuilder = WebClient.builder();

    @Value("${spring.security.oauth2.resourceserver.jwt.jwk-set-uri}")
    private String jwtSetUri;

    @Value("${spring.security.oauth2.resourceserver.jwt.keycloak-login-endpoint}")
    private String keycloakLoginUrl;

    @Value("${spring.security.oauth2.resourceserver.jwt.keycloak-login-client-id}")
    private String keycloakClientId;

    @Value("${spring.security.oauth2.resourceserver.jwt.keycloak-username}")
    private String applicationKeycloakUsername;

    @Value("${spring.security.oauth2.resourceserver.jwt.keycloak-password}")
    private String applicationKeycloakPassword;


    public void updateToken(){
        //if token is null or expired
        if(Objects.isNull(jwt) || jwt.getExpiresAt().isBefore(Instant.now().minusSeconds(30L))){

            MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
            formData.add("grant_type", "password");
            formData.add("client_id", keycloakClientId);
            formData.add("username", applicationKeycloakUsername);
            formData.add("password", applicationKeycloakPassword);

            KeycloakLoginResponse keycloakLoginResponse = webClientBuilder.build()
                    .post()
                    .uri(keycloakLoginUrl)
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .body(BodyInserters.fromFormData(formData))
                    .retrieve()
                    .bodyToMono(KeycloakLoginResponse.class)
                    .block();

            JwtDecoder jwtDecoder = NimbusJwtDecoder.withJwkSetUri(jwtSetUri).build();

            if(Objects.isNull(keycloakLoginResponse.getAccess_token())){
                throw new RuntimeException("Failed to retrieve access token from keycloak response");
            }

            Jwt jwtFromKeycloak = jwtDecoder.decode(keycloakLoginResponse.getAccess_token());
            setJwt(jwtFromKeycloak);
        }
    }

    public String getApplicationBearerToken(){
        updateToken();
        return jwt.getTokenValue();
    }

}
