package com.jbdev.library.keycloakintegration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Slf4j
public class JwtAuthConverter implements Converter<Jwt, AbstractAuthenticationToken> {

    private final JwtGrantedAuthoritiesConverter jwtConverter = new JwtGrantedAuthoritiesConverter();

    private final String keycloakTokenUsernameKey = "preferred_username";

    @Override
    public AbstractAuthenticationToken convert(@NonNull Jwt jwt) {
        Collection<GrantedAuthority> authorities = Stream.concat(
                jwtConverter.convert(jwt).stream(),
                extractResourceRoles(jwt).stream()
        ).collect(Collectors.toSet());

        String principalName = getPrincipalClaimName(jwt);

        //Filter keycloak specific roles
        List<GrantedAuthority> filteredAuthorities = authorities.stream().filter(authority ->
                        !authority.getAuthority().equals("ROLE_offline_access") &&
                        !authority.getAuthority().equals("SCOPE_email") &&
                        !authority.getAuthority().equals("ROLE_uma_authorization") &&
                        !authority.getAuthority().equals("ROLE_client_user") &&
                        !authority.getAuthority().equals("SCOPE_profile") &&
                        !authority.getAuthority().equals("ROLE_default-roles-master")
                ).toList();
        //

        log.info("--- Authentication and Authorization info for incoming request ---");
        log.info("Pincipal name: " + principalName);
        log.info("Authorities: " + filteredAuthorities.toString());
        log.info("--- --- --- --- ---");


        return new JwtAuthenticationToken(
                jwt,
                filteredAuthorities,
                principalName
        );
    }

    private String getPrincipalClaimName(Jwt jwt) {
        return jwt.getClaim(keycloakTokenUsernameKey);
    }

    private Collection<? extends GrantedAuthority> extractResourceRoles(Jwt jwt) {
        Map<String, Object> resourceAccess;
        Collection<String> resourceRoles;
        if(jwt.getClaim("realm_access") == null){
            return Set.of();
        }

        resourceAccess = jwt.getClaim("realm_access");
        resourceRoles = (Collection<String>)resourceAccess.get("roles");

        return resourceRoles.stream()
                .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
                .collect(Collectors.toSet());
    }

}
