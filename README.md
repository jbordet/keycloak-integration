This is a Keycloak version 24.0 integration dependency for a springboot project using Java 17+. 

To set up:
1) Clone repository
2) Execute a mvn clean install
3) Create/Open your springboot project
4) Add the dependency
~~~
		<dependency>
			<groupId>com.jbdev.library</groupId>
			<artifactId>keycloak-integration</artifactId>
			<version>0.0.4-SNAPSHOT</version>
		</dependency>
~~~

5) Declare the following application properties:
~~~
## Keycloak integration config ##

**THIS PROPERTY VALUE NEEDS TO MATCH THE ACTUAL JWT value for the attribute "iss". Before setting up this value log in to keycloak with user and password, get a token, put the token in jwt.io and check the value of "iss", copy and paste it here, its expected that it may be different than the one below -> jwk-set-uri, since this value purpose is to check when a JWT arrives if its "iss" is equal to this, if not it is discarded/no evaluated and a 401 Unauthorized is thrown**
spring.security.oauth2.resourceserver.jwt.issuer-uri=http://localhost:8090/realms/master


This property indicates to Springboot against which URL should it validate the token, so the "localhost:8090" here should point to the actual public ip of the keycloak server instance
spring.security.oauth2.resourceserver.jwt.jwk-set-uri=http://localhost:8090/realms/master/protocol/openid-connect/certs


spring.security.oauth2.resourceserver.jwt.keycloak-login-endpoint=http://localhost:8090/realms/master/protocol/openid-connect/token
#Here react-client should be the name of the keyclok client within the realm
spring.security.oauth2.resourceserver.jwt.keycloak-login-client-id=react-client

#These are the username and password credentials for the application (backend service) user
spring.security.oauth2.resourceserver.jwt.keycloak-username=pepito
spring.security.oauth2.resourceserver.jwt.keycloak-password=123456
~~~

6) In your @SpringbootApplication class add the following annotation:

~~~
@ComponentScan(basePackages = {
        "com.jbdev.library.keycloakintegration",
        "{your-local-project-base-package}"
})
~~~